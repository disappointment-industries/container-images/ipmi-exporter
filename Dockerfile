FROM debian
ARG TARGETARCH
RUN DEBIAN_FRONTEND=noninteractive apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y freeipmi
WORKDIR /app
EXPOSE 9290
COPY ipmi_exporter-${TARGETARCH} /app/ipmi_exporter
ENTRYPOINT [ "/app/ipmi_exporter" ]
